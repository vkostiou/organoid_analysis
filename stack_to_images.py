#!/usr/bin/env python
import argparse
import datetime
import logging
import textwrap
from pathlib import Path

from stack import Stack, Mosaic_Stack, is_mosaic_stack


def main():
    args = parse_arguments()

    logger = logging.getLogger("logger")
    configure_logger(logger, args)

    if not is_valid_stack_file(args.input_stack):
        logger.warning("No valid input stack found!")
    logger.info(f"Stack filename: {args.input_stack}")
    logger.info(f"Destination directory: {args.workdir}")
    logger.info(f"Cell type: {args.cell_type}")

    if is_mosaic_stack(args.input_stack):
        stack = Mosaic_Stack(args.input_stack, args.cell_type)
    else:
        stack = Stack(args.input_stack, args.cell_type)

    logger.info(f"Saving slice images for {args.input_stack} in {args.workdir}... ")
    stack.save_slice_images(args.workdir)

    logger.info('Successfully completed!')


def configure_logger(logger, args):
    logger.setLevel(logging.DEBUG)

    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)

    now = datetime.datetime.now()
    date_time = now.strftime("%Y%m%d_%H%M%S")
    file_handler = logging.FileHandler(
        args.workdir / Path(f"{date_time}_stack_to_images.log"))
    file_handler.setLevel(logging.INFO)

    log_format = logging.Formatter(
        '%(asctime)s - %(levelname)s - %(module)s:%(funcName)s - %(message)s')
    console_handler.setFormatter(log_format)
    file_handler.setFormatter(log_format)

    logger.addHandler(console_handler)
    logger.addHandler(file_handler)


def is_valid_stack_file(file: Path) -> bool:
    valid_extensions = ['.czi']
    return file.suffix in valid_extensions


def parse_arguments() -> argparse.Namespace:
    logger = logging.getLogger("logger")

    def validate_arg(f: str) -> str:
        if not Path(f).exists():
            error_msg = f"{f} does not exist"
            logger.error(error_msg)
            raise argparse.ArgumentTypeError(error_msg)
        return f

    description = textwrap.dedent('''
    This script takes a stack file as input and saves to individual slice images.
    Output is stored in a specific analysis working directory provided by the user.
    The output directory has the following structure: <workdir>/<stack name>/<scene>/<time-point>/<channel>/
    ''')
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=description)
    required_named = parser.add_argument_group('Required named arguments')
    required_named.add_argument('-i', '--input_stack', help='Input stack file',
                                required=True, type=validate_arg, metavar='PATH')
    required_named.add_argument('-c', '--cell_type', help='cell type (e.g. Barretts)', required=True,
                                metavar='CELL_TYPE')
    required_named.add_argument('-w', '--workdir', type=validate_arg, metavar='PATH', required=True,
                        help='Analysis working directory')
    args = parser.parse_args()

    args.input_stack = Path(args.input_stack).resolve()
    args.workdir = Path(args.workdir).resolve()

    return args


if __name__ == "__main__":
    main()

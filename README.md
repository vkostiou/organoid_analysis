# Introduction

This repository contains the source code for the core functionality of the automated organoid image analysis pipeline.
The pipeline consists of three main steps:
1. Split an input z-stack file to individual slice images. The pipeline allows the analysis of Mosaic stacks as well.
2. Detect individual organoids within each slice.
3. Annotate detected organoids by quantifying their shape and count total organoid population.

![Pipeline Overview](pipeline_overview.png)

# How to run

## Run in a single step using the unified workflow

To facilitate the analysis, the organoid analysis pipeline has been also implemented as a single workflow. The source code and further instructions can be found at the [Organoid Pipeline](https://gitlab.com/vkostiou/organoid_pipeline.git) repository.

## Run each step separately

Each step can be run separately in two ways:
- Using the docker containers
    >Install [Docker engine](https://docs.docker.com/get-docker/)
- Installing dependencies and running the Python scripts
    >It is strongly recommended to create a virtual environment using **Python 3.8** before installing the required packages.

    `$ pip install requirements.txt`

### Step 1 (Split z-stack to slice images)

<ins>**Option 1: Use the docker container:**</ins>

    $ STACK_DIR=/absolute/path/to/dir/containing/input/stack
    $ WORK_DIR=/absolute/path/to/your/preferred/results/dir
    $ docker run --mount type=bind,source=$STACK_DIR,target=/input_stack_dir --mount type=bind,source=$WORK_DIR,target=/workdir registry.gitlab.com/vkostiou/orgaquant_analysis stack_to_images.py -i /input_stack_dir/MYSTACK.czi -w /workdir -c 'MY_CELL_TYPE'

<ins>**Option 2: Run the Python script:**</ins>

    usage: stack_to_images.py [-h] -i PATH -c CELL_TYPE -w PATH

    This script takes a stack file as input and saves to individual slice images.
    Output is stored in a specific analysis working directory provided by the user.
    The output directory has the following structure: <workdir>/<stack name>/<scene>/<time-point>/<channel>/

    optional arguments:
        -h, --help            show this help message and exit

    Required named arguments:
        -i PATH, --input_stack PATH
                        Input stack file
        -c CELL_TYPE, --cell_type CELL_TYPE
                        cell type (e.g. Barretts)
        -w PATH, --workdir PATH
                        Analysis working directory

###  Step 2 (Detect organoids within slices)

This step performs automated organoid detection. For detailed instructions on how to run this step please see [here](https://gitlab.com/vkostiou/orgaquant_headless.git).  

###  Step 3 (Annotate organoids)

<ins>**Option 1: Use the docker container:**</ins>

    $ STACK_DIR=/absolute/path/to/dir/containing/input/stack
    $ WORK_DIR=/absolute/path/to/your/preferred/results/dir
    $ docker run --mount type=bind,source=$STACK_DIR,target=/input_stack_dir --mount type=bind,source=$WORK_DIR,target=/workdir registry.gitlab.com/vkostiou/orgaquant_analysis organoid_analysis.py -i /input_stack_dir/MYSTACK.czi -w /workdir -c 'MY_CELL_TYPE'

<ins>**Option 2: Run the Python script:**</ins>

    usage: organoid_analysis.py [-h] -i PATH -c CELL_TYPE -w PATH [-f INT]

    This script performs organoid analysis of a stack file by calling the necessary downstream functions.
    It takes as input a single czi file.

    optional arguments:
        -h, --help              show this help message and exit
        -f INT, --focus_threshold INT
                                Focus threshold value, default: 19

    Required named arguments:
        -i PATH, --input_stack PATH
                                Input stack file
        -c CELL_TYPE, --cell_type CELL_TYPE
                                cell type (e.g. Barretts)
        -w PATH, --workdir PATH
                                Analysis working directory

# Pipeline Output

The directory structure of input and output files in the image analysis pipeline is organised in the following way:

    $ $WORKDIR=user's/preferred/directory/to/save/pipeline/outputs
    $ $WORKDIR/<stack name>/<scene>/<time point>/Bright/orgaquant_output/<parameters>/

The organoid analysis pipeline produces two output types:
1. A summary Comma Separated Values (CSV) file containing all the identified organoids along with their quantification analysis results. This is saved inside the `$WORKDIR`.
2. Images containing the aggregated labels of all the in-focus organoids (`*_ANNOTATED.png`).These are saved in the following location:
`$WORKDIR/<stack name>/<scene>/<time point>/Bright/orgaquant_output/<parameters>/`.

import re
import sys
from pathlib import Path

import cv2
import imutils
import numpy as np
import pandas as pd
from aicsimageio import AICSImage
from aicsimageio.writers import two_d_writer

from typing import Dict, NoReturn


def is_mosaic_stack(path: str) -> bool:
    aics = AICSImage(path, reconstruct_mosaic=False)
    if len(aics.shape) == 6:
        return True
    elif len(aics.shape) == 5:
        return False
    else:
        raise Exception(f'Incompatible Stack shape!')


class Stack(object):
    def __init__(self, path: Path, cell_type: str):
        self.aics = AICSImage(path.resolve(), reconstruct_mosaic=False)
        self.fullname = path.name
        self.name = path.stem
        self.cell_type = cell_type
        self.fullpath = path.resolve()  # TODO unused
        self.num_of_scenes = len(self.aics.scenes)

        self.read_aics_shape()

        (self.z_pixel_to_microns, self.y_pixel_to_microns, self.x_pixel_to_microns) = self.aics.physical_pixel_sizes

        if self.x_pixel_to_microns != self.y_pixel_to_microns:
            sys.exit()  # TODO implement logger
        # if x_pixel_to_microns == 1.0 or y_pixel_to_microns == 1.0:
        #     sys.exit()  # TODO implement logger

        self.channel_names = self.aics.channel_names
        self.__set_day()

    def read_aics_shape(self):
        (self.num_of_timepoints,
         self.num_of_channels,
         self.num_of_slices,
         self.y_res,
         self.x_res) = self.aics.shape

    def is_mosaic(self):
        return False

    def __create_dir_structure(self, root_dir: Path) -> NoReturn:
        for scene in range(1, self.num_of_scenes + 1):
            for timepoint in range(1, self.num_of_timepoints + 1):
                for channel in self.channel_names:
                    dir = root_dir / Path(f"{self.name}/{scene}/{timepoint}/{channel}")
                    try:
                        dir.mkdir(parents=True)
                    except FileExistsError:
                        print("Directory ", dir, " already exists")  # TODO convert to logger and raise error

    def __set_day(self) -> NoReturn:
        self.day = re.findall(r'.+day\s+(\d+)', self.name)
        if not self.day:
            self.day = re.findall(r'.+(\d+)\sdays', self.name)

    def get_Slices(self, num_of_tiles=0) -> Dict:
        slices = {}

        for s in range(self.num_of_scenes):
            self.aics.set_scene(s)
            for t in range(self.num_of_timepoints):
                for ch in range(self.num_of_channels):
                    scene = s + 1
                    timepoint = t + 1
                    channel_name = self.channel_names[ch]
                    slices.setdefault(scene, {}).setdefault(timepoint, {}).setdefault(channel_name, [])

                    for sl in range(self.num_of_slices):
                        slice_number = sl + 1
                        if not self.is_mosaic():
                            slice_image_data = self.aics.get_image_data("YX", T=t, C=ch, Z=sl)
                            slices[scene][timepoint][channel_name].append(
                                Slice(slice_image_data, scene, timepoint, channel_name, slice_number, self))
                        else:
                            for m in range(num_of_tiles):
                                tile_number = m + 1
                                tile_image_data = self.aics.get_image_data("YX", T=t, C=ch, Z=sl, M=m)
                                temp_obj = Tile(tile_number, tile_image_data, scene, timepoint, channel_name,
                                                slice_number, self)
                                slices[scene][timepoint][channel_name].append(temp_obj)

        return slices

    def get_Slice_by_scene_timepoint_channel_name_slice_number(self, scene: int, timepoint: int, channel_name: str,
                                                               slice_number: int):
        channel = self.channel_names.index(channel_name)
        self.aics.set_scene(scene - 1)
        slice_image_data = self.aics.get_image_data("YX", T=timepoint - 1, C=channel, Z=slice_number - 1)
        return Slice(slice_image_data, scene, timepoint, channel_name, slice_number, self)

    def save_slice_images(self, destination_dir: Path) -> NoReturn:
        self.__create_dir_structure(destination_dir)
        for scene, timepoints in self.get_Slices().items():
            for timepoint, channels in timepoints.items():
                for channel, slices in channels.items():
                    for slice in slices:
                        image_path = destination_dir / Path(
                            f"{self.name}/{slice.scene}/{slice.timepoint}/{slice.channel_name}/{slice.fullname}")
                        two_d_writer.TwoDWriter.save(slice.image_data, image_path)


class Mosaic_Stack(Stack):
    def is_mosaic(self):
        return True

    def read_aics_shape(self):
        (self.num_of_tiles,
         self.num_of_timepoints,
         self.num_of_channels,
         self.num_of_slices,
         self.y_res,
         self.x_res) = self.aics.shape

    def get_Slices(self, num_of_tiles=0) -> Dict:
        return self.get_Tiles()

    def get_Tiles(self) -> Dict:
        return super(self.__class__, self).get_Slices(num_of_tiles=self.num_of_tiles)

    def get_Slice_by_scene_timepoint_channel_name_slice_number(self, scene: int, timepoint: int, channel_name: str,
                                                               slice_number: int):
        raise Exception(
            f'This is a Mosaic Stack! {self.get_Tile_by_scene_timepoint_channel_name_slice_number_tile_number.__name__} should be called instead!')

    def get_Tile_by_scene_timepoint_channel_name_slice_number_tile_number(self, scene: int, timepoint: int,
                                                                          channel_name: str,
                                                                          slice_number: int, tile_number: int):
        channel = self.channel_names.index(channel_name)
        self.aics.set_scene(scene - 1)
        tile_image_data = self.aics.get_image_data("YX", T=timepoint - 1, C=channel, Z=slice_number - 1,
                                                   M=tile_number - 1)
        return Tile(tile_number, tile_image_data, scene, timepoint, channel_name, slice_number, self)


class Slice(object):
    # TODO should these be hardcoded?
    EDGE_PADDING = 10
    ORGANOID_AREA_THRESHOLD = 1500000
    COLOUR_CLASSIFICATION_THRESHOLD = 0

    def __init__(self, slice_image_data, scene, timepoint, channel_name, slice_number, stack):
        self.image_data = slice_image_data
        self.fullname = stack.name + '_s' + str(scene) + '_t' + str(timepoint) + '_z' + str(slice_number) + '.png'
        self.name = Path(self.fullname).stem
        self.stack = stack
        self.scene = scene
        self.timepoint = timepoint
        self.channel_name = channel_name
        self.slice_number = slice_number
        self.has_organoids = False

    def get_rgb_image_data(self) -> np.ndarray:
        return cv2.cvtColor(self.image_data, cv2.COLOR_GRAY2RGB)

    def get_composite_image_data(self) -> np.ndarray:

        if self.channel_name == 'Bright':
            composite_image_data = self.get_rgb_image_data()

            fluorescence_channel_to_Slices = self.__get_fluoresence_Slices()

            for fluo_channel_name, fluo_Slice in fluorescence_channel_to_Slices.items():
                bgr_fluo_image_data = []
                zero_array = np.zeros(fluo_Slice.image_data.shape, dtype=np.uint8)
                if fluo_channel_name == 'mCher':
                    bgr_fluo_image_data = cv2.merge((zero_array, zero_array, fluo_Slice.image_data))
                if fluo_channel_name == 'EGFP':
                    bgr_fluo_image_data = cv2.merge((zero_array, fluo_Slice.image_data, zero_array))

                composite_image_data = cv2.add(composite_image_data, bgr_fluo_image_data)

            return composite_image_data

    def set_organoids(self, workdir: Path, parameter_string: str, focus_threshold: float) -> NoReturn:
        slice_image_path = Path(workdir.resolve(), self.stack.name, str(self.scene), str(self.timepoint),
                                        self.channel_name,
                                        self.fullname)
        csv_path = Path(workdir.resolve(), self.stack.name, str(self.scene), str(self.timepoint),
                                self.channel_name, 'orgaquant_output', parameter_string,
                                self.fullname + '.csv')
        df = pd.read_csv(csv_path)
        self.organoids = df.loc[:, ["x1", "y1", "x2", "y2", "Diameter 1 (Pixels)",
                                    "Diameter 2 (Pixels)"]]  # TODO is this necessary

        if self.organoids.shape[0] > 0:
            self.has_organoids = True
            self.__add_diameters_in_microns()
            self.__add_organoid_cell_type()
            self.__add_scene_timepoint_slice()
            self.__add_organoid_bounding_box_centre()
            self.__add_organoid_bounding_box_area()
            self.__add_organoid_custom_id()
            self.__add_fluorescence()
            self.__classify_organoids_by_colour()
            self.__add_pixel_intensity()
            self.__split_to_organoid_size_groups()
            self.__add_organoid_focus_metric(str(slice_image_path))
            self.__remove_large_edge()
            self.__process_overlapping_organoids()
            self.__remove_blurry_organoids(focus_threshold)

    def __add_diameters_in_microns(self) -> NoReturn:
        self.organoids['Diameter 1 (Microns)'] = \
            (self.stack.x_pixel_to_microns * self.organoids["Diameter 1 (Pixels)"])
        self.organoids['Diameter 2 (Microns)'] = \
            (self.stack.x_pixel_to_microns * self.organoids["Diameter 2 (Pixels)"])

    def __add_organoid_cell_type(self) -> NoReturn:
        if self.stack.cell_type == 'competition':
            pass
        else:
            self.organoids['cell_type'] = self.stack.cell_type

    def __add_scene_timepoint_slice(self) -> NoReturn:
        self.organoids['scene'] = self.scene
        self.organoids['timepoint'] = self.timepoint
        self.organoids['slice'] = self.slice_number

    def __add_organoid_bounding_box_centre(self) -> NoReturn:
        x = (self.organoids['x1'] + self.organoids['x2']) / 2
        y = (self.organoids['y1'] + self.organoids['y2']) / 2

        self.organoids['xcentre'] = x
        self.organoids['ycentre'] = y

    def __add_organoid_bounding_box_area(self) -> NoReturn:
        area = (abs(self.organoids['x2'] - self.organoids['x1'])) * (abs(self.organoids['y2'] - self.organoids['y1']))

        self.organoids['bb_area'] = area

    def __add_organoid_custom_id(self) -> NoReturn:
        self.organoids['id'] = 's' + str(self.scene) + 't' + str(self.timepoint) + 'z' + str(self.slice_number) \
                               + '_' + self.organoids.index.astype(str)

    def __add_fluorescence(self) -> NoReturn:
        channel_to_Slices = self.__get_fluoresence_Slices()

        for index, org in self.organoids.iterrows():
            for channel_name, fluo_slice in channel_to_Slices.items():
                fluo_organoid_image = fluo_slice.image_data[org['y1']:org['y2'], org['x1']:org['x2']]
                self.organoids.at[index, channel_name + '_intensity'] = fluo_organoid_image.mean()

    def __get_fluoresence_Slices(self):
        channel_to_Slices = {}
        for channel_name in self.stack.channel_names:
            if channel_name != self.channel_name:
                channel_to_Slices[channel_name] = self.stack.get_Slice_by_scene_timepoint_channel_name_slice_number(
                    self.scene, self.timepoint, channel_name, self.slice_number)
        return channel_to_Slices

    def __add_pixel_intensity(self) -> NoReturn:
        for index, org in self.organoids.iterrows():
            organoid_image = self.image_data[org['y1']:org['y2'], org['x1']:org['x2']]
            self.organoids.at[index, 'mean_intensity'] = organoid_image.mean()
            self.organoids.at[index, 'median_intensity'] = np.median(organoid_image)

    def __classify_organoids_by_colour(self) -> NoReturn:
        channel_names = self.stack.channel_names[:]
        channel_names.remove('Bright')

        if len(channel_names) == 1:
            self.organoids['colour'] = channel_names[0]
        elif len(channel_names) == 2:
            [colour_one, colour_two] = channel_names
            bit_depth = 8  # TODO retrieve from ndarray.dtype (self.image_data.dtype)
            self.organoids['colour'] = np.where(
                abs(self.organoids[colour_one + '_intensity'] - self.organoids[colour_two + '_intensity']) * 100 / (
                        2 ** bit_depth) <= self.COLOUR_CLASSIFICATION_THRESHOLD, 'mixed', (
                    np.where(self.organoids[colour_one + '_intensity'] > self.organoids[colour_two + '_intensity'],
                             colour_one, colour_two)))

    def __detect_blur_fft(self, image: np.ndarray):
        orig = imutils.resize(image, width=500)
        gray = cv2.cvtColor(orig, cv2.COLOR_BGR2GRAY)
        size = 5  # TODO add comment regarding this value
        # grab the dimensions of the image and use the dimensions to
        # derive the center (x, y)-coordinates
        (h, w) = gray.shape
        (cX, cY) = (int(w / 2.0), int(h / 2.0))

        # compute the FFT to find the frequency transform, then shift
        # the zero frequency component (i.e., DC component located at
        # the top-left corner) to the center where it will be more
        # easy to analyze
        fft = np.fft.fft2(gray)
        fftShift = np.fft.fftshift(fft)

        # zero-out the center of the FFT shift (i.e., remove low
        # frequencies), apply the inverse shift such that the DC
        # component once again becomes the top-left, and then apply
        # the inverse FFT
        fftShift[cY - size:cY + size, cX - size:cX + size] = 0
        fftShift = np.fft.ifftshift(fftShift)
        recon = np.fft.ifft2(fftShift)

        # compute the magnitude spectrum of the reconstructed image,
        # then compute the mean of the magnitude values
        magnitude = 20 * np.log(np.abs(recon))
        mean = np.mean(magnitude)

        return mean

    def __split_to_organoid_size_groups(self) -> NoReturn:
        small_threshold = self.organoids.bb_area.quantile(0.25)
        medium_threshold = self.organoids.bb_area.quantile(0.75)

        self.organoids.loc[self.organoids['bb_area'] <= small_threshold, 'size_group'] = 'small'
        self.organoids.loc[(self.organoids['bb_area'] > small_threshold) & (
                self.organoids['bb_area'] <= medium_threshold), 'size_group'] = 'medium'
        self.organoids.loc[self.organoids['bb_area'] > medium_threshold, 'size_group'] = 'large'

        size_groups = self.organoids['size_group'].unique()

        for size_group in size_groups:
            mean_width = self.organoids.loc[self.organoids['size_group'] == size_group, 'Diameter 2 (Pixels)'].mean()
            self.organoids.loc[self.organoids['size_group'] == size_group, 'group_mean_width'] = mean_width
            mean_length = self.organoids.loc[self.organoids['size_group'] == size_group, 'Diameter 1 (Pixels)'].mean()
            self.organoids.loc[self.organoids['size_group'] == size_group, 'group_mean_length'] = mean_length

    def __add_organoid_focus_metric(self, slice_image_path: str) -> NoReturn:
        slice_image = cv2.imread(slice_image_path)  # TODO replace reading from disk with getter method

        for index, org in self.organoids.iterrows():
            organoid_image = slice_image[org['y1']:org['y2'], org['x1']:org['x2']]
            focus_metric = self.__detect_blur_fft(organoid_image)
            self.organoids.at[index, 'focus_metric'] = focus_metric

    def __remove_large_edge(self) -> NoReturn:
        ydim = self.image_data.shape[0]
        xdim = self.image_data.shape[1]
        self.organoids = self.organoids[(self.organoids['x1'] > self.EDGE_PADDING)
                                        & (self.organoids['x2'] < xdim - self.EDGE_PADDING)
                                        & (self.organoids['y1'] > self.EDGE_PADDING)
                                        & (self.organoids['y2'] < ydim - self.EDGE_PADDING)
                                        & (self.organoids[
                                               'bb_area'] < self.ORGANOID_AREA_THRESHOLD)]  # TODO remove outliers

    def __get_overlap_area(self, idx1, idx2):
        axmin = self.organoids.loc[idx1, 'x1']  # org1
        aymin = self.organoids.loc[idx1, 'y1']
        axmax = self.organoids.loc[idx1, 'x2']
        aymax = self.organoids.loc[idx1, 'y2']

        bxmin = self.organoids.loc[idx2, 'x1']  # org2
        bymin = self.organoids.loc[idx2, 'y1']
        bxmax = self.organoids.loc[idx2, 'x2']
        bymax = self.organoids.loc[idx2, 'y2']

        dx = min(axmax, bxmax) - max(axmin, bxmin)
        dy = min(aymax, bymax) - max(aymin, bymin)
        if (dx >= 0) and (dy >= 0):
            return dx * dy
        else:
            return 0

    def __process_overlapping_organoids(self) -> NoReturn:
        unwanted_indexes = set()
        organoid_indexes = list(self.organoids.index.values)
        for first_organoid in organoid_indexes:
            for second_organoid in organoid_indexes:
                if first_organoid == second_organoid:
                    continue
                if self.organoids.loc[first_organoid, 'bb_area'] < self.organoids.loc[second_organoid, 'bb_area']:
                    (smaller_org_idx, larger_org_idx) = (first_organoid, second_organoid)
                else:
                    (smaller_org_idx, larger_org_idx) = (second_organoid, first_organoid)

                overlap_area = self.__get_overlap_area(first_organoid, second_organoid)

                if overlap_area / self.organoids.loc[smaller_org_idx, 'bb_area'] > 0.5:
                    if self.organoids.loc[smaller_org_idx, 'focus_metric'] \
                            < self.organoids.loc[larger_org_idx, 'focus_metric']:
                        unwanted_indexes.add(smaller_org_idx)
                    else:
                        unwanted_indexes.add(larger_org_idx)

        self.organoids = self.organoids.drop(list(unwanted_indexes), axis=0)

    def __remove_blurry_organoids(self, threshold: float) -> NoReturn:
        self.organoids = self.organoids[(self.organoids['focus_metric'] > threshold)]
        # If there are no organoids left, update the relevant field
        if self.organoids.shape[0] == 0:
            self.has_organoids = False

    def save_annotated_image(self, destination_dir: Path, parameters: str) -> NoReturn:
        image = self.get_composite_image_data()
        for index, org in self.organoids.iterrows():
            image = cv2.putText(image, str(index), (org['x1'], org['y1'] - 5), cv2.FONT_HERSHEY_SIMPLEX, 1,
                                (127, 0, 255), 2)
            image = cv2.rectangle(image, (org['x1'], org['y1']), (org['x2'], org['y2']), (127, 0, 255), 2)

        cv2.imwrite(str(Path(destination_dir.resolve(), self.stack.name, str(self.scene), str(self.timepoint),
                                 'Bright', 'orgaquant_output', parameters, self.name + '_ANNOTATED.png')), image)


class Tile(Slice):
    def __init__(self, tile_number, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.tile_number = tile_number
        self.fullname = self.stack.name + '_s' + str(self.scene) + '_t' + str(self.timepoint) + '_z' + str(
            self.slice_number) + '_m' + str(tile_number) + '.png'
        self.name = Path(self.fullname).stem

    def __add_tile(self) -> NoReturn:
        tile_column_index = self.organoids.columns.get_loc('slice') + 1
        self.organoids.insert(tile_column_index, 'tile', self.tile_number)

    def __add_organoid_custom_id(self) -> NoReturn:
        self.organoids['id'] = 's' + str(self.scene) + 't' + str(self.timepoint) + 'z' + str(self.slice_number) \
                               + 'm' + str(self.tile_number) + '_' + self.organoids.index.astype(str)

    def set_organoids(self, *args, **kwargs) -> NoReturn:
        super(self.__class__, self).set_organoids(*args, **kwargs)
        if self.organoids.shape[0] > 0:
            self.__add_tile()
            self.__add_organoid_custom_id()
            self.__add_fluorescence()

    def __add_fluorescence(self) -> NoReturn:
        channel_to_Slices = self.__get_fluoresence_Slices()

        for index, org in self.organoids.iterrows():
            for channel_name, fluo_slice in channel_to_Slices.items():
                fluo_organoid_image = fluo_slice.image_data[org['y1']:org['y2'], org['x1']:org['x2']]
                self.organoids.at[index, channel_name + '_intensity'] = fluo_organoid_image.mean()

    def __get_fluoresence_Slices(self):
        return self.__get_fluoresence_Tiles()

    def __get_fluoresence_Tiles(self):
        channel_to_Tiles = {}
        for channel_name in self.stack.channel_names:
            if channel_name != self.channel_name:
                channel_to_Tiles[
                    channel_name] = self.stack.get_Tile_by_scene_timepoint_channel_name_slice_number_tile_number(
                    self.scene, self.timepoint, channel_name, self.slice_number, self.tile_number)
        return channel_to_Tiles

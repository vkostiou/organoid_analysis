import argparse
import textwrap
import datetime
import logging
import sys
from pathlib import Path

import numpy as np
import pandas as pd
from tqdm import tqdm
from typing import Union, List, NoReturn
from pandas.core.groupby import SeriesGroupBy, DataFrameGroupBy
from sklearn.cluster import AgglomerativeClustering

from stack import Stack, Slice, Mosaic_Stack, Tile, is_mosaic_stack

CLUSTERING_DISTANCE_THRESHOLD_IN_PIXELS = 20


def cluster_organoids(slices: List[Slice]) -> Union[SeriesGroupBy, DataFrameGroupBy]:
    bb_centres = []
    organoid_ids = []
    all_organoids = [slice.organoids for slice in slices]
    unified_organoids_df = pd.concat(all_organoids)
    organoid_centres = unified_organoids_df[["id", "xcentre", "ycentre"]].set_index("id").T.to_dict("list")

    for id, centre in organoid_centres.items():
        organoid_ids.append(id)
        bb_centres.append(np.array(centre))

    clustering = AgglomerativeClustering(linkage='ward', distance_threshold=CLUSTERING_DISTANCE_THRESHOLD_IN_PIXELS,
                                         n_clusters=None).fit(bb_centres)

    for index, cluster_id in enumerate(clustering.labels_):
        unified_organoids_df.loc[unified_organoids_df['id'] == organoid_ids[index], 'cluster'] = cluster_id

    clustered_organoids = unified_organoids_df.groupby('cluster')

    # TODO check that there are no members that belong to the same slice

    return clustered_organoids


def get_Slices_with_organoids(slices: List[Slice]) -> List[Slice]:
    return [slice for slice in slices if slice.has_organoids is True]


def get_unwanted_organoid_ids(clustered_organoids: DataFrameGroupBy, is_mosaic: bool):
    unwanted_organoid_ids = []
    for cluster_id, members in clustered_organoids:
        if members.shape[0] > 1:
            members.sort_values(by=['slice'], inplace=True)
            subgroups = dict(tuple(members.groupby(members['slice'].diff().gt(1).cumsum())))
            for subgroup in subgroups.values():
                if subgroup.shape[0] > 1:
                    if is_mosaic:
                        subgroup = subgroup[subgroup.duplicated('tile', keep=False)]
                    max_focus_metric = subgroup['focus_metric'].max()
                    unwanted_organoid_ids.extend(
                        subgroup.loc[subgroup['focus_metric'] != max_focus_metric, 'id'].tolist())

    return unwanted_organoid_ids


def remove_unwanted_organoids(slices: List[Slice], unwanted_organoid_ids: List) -> NoReturn:
    for slice in slices:
        slice.organoids = slice.organoids[~slice.organoids['id'].isin(unwanted_organoid_ids)]
        # If there are no organoids left, update the relevant field
        if slice.organoids.shape[0] == 0:
            slice.has_organoids = False


def keep_unique_organoids_across_slices(slices: List[Slice]) -> NoReturn:
    clustered_organoids = cluster_organoids(slices)
    is_mosaic = slices[0].stack.is_mosaic()
    unwanted_organoid_ids = get_unwanted_organoid_ids(clustered_organoids, is_mosaic)
    remove_unwanted_organoids(slices, unwanted_organoid_ids)


def main():
    args = parse_arguments()

    logger = logging.getLogger("logger")
    configure_logger(logger, args)

    if is_mosaic_stack(args.input_stack):
        stack = Mosaic_Stack(args.input_stack, args.cell_type)
        logger.info(f'Stack is mosaic')
    else:
        stack = Stack(args.input_stack, args.cell_type)

    stack_slices = stack.get_Slices()
    parameter_combinations = [param_comb.name for param_comb in Path(args.workdir, stack.name, '1', '1', 'Bright', 'orgaquant_output').glob('*/')]
    if not parameter_combinations:
        error_msg = f'No OrgaQuant output found in {args.workdir}'
        logger.error(error_msg)
        sys.exit(error_msg)

    all = len(parameter_combinations) * stack.num_of_scenes * stack.num_of_timepoints * stack.num_of_slices
    if stack.is_mosaic():
        all = all * stack.num_of_tiles

    with tqdm(total=all) as pbar:
        for parameter_combination in parameter_combinations:
            logger.info(f'Analysing organoids for parameters: {parameter_combination}...')
            all_stack_organoid_dfs = []
            for scene, timepoints in stack_slices.items():
                for timepoint, channel_names in timepoints.items():
                    for slice in stack_slices[scene][timepoint]['Bright']:
                        slice.set_organoids(args.workdir, parameter_combination, args.focus_threshold)
                        pbar.update(1)

                    slices_with_organoids = get_Slices_with_organoids(stack_slices[scene][timepoint]['Bright'])
                    keep_unique_organoids_across_slices(slices_with_organoids)
                    slices_with_organoids = get_Slices_with_organoids(stack_slices[scene][timepoint]['Bright'])

                    for slice in slices_with_organoids:
                        new_column_index = slice.organoids.columns.get_loc('xcentre')
                        slice.organoids.insert(new_column_index, 'annotation_id', slice.organoids.index)
                        slice.save_annotated_image(args.workdir, parameter_combination)
                        all_stack_organoid_dfs.append(slice.organoids)

            # Create unique indices across all organoid dataframes
            res = pd.concat(all_stack_organoid_dfs)
            res.reset_index(inplace=True, drop=True)
            res.index += 1
            res.to_csv(Path(args.workdir, stack.name + '_' + parameter_combination + '.csv'))

    logger.info('Organoid analysis successfully completed!')


def parse_arguments() -> argparse.Namespace:
    logger = logging.getLogger("logger")

    def validate_arg(f):
        if not Path(f).exists():
            error_msg = f'{f} does not exist'
            logger.error(error_msg)
            raise argparse.ArgumentTypeError(error_msg)
        return f

    def validate_cell_type(ct):
        valid_cell_types = ['barretts', 'cancer', 'mixed']
        if ct.lower() not in valid_cell_types:
            error_msg = f'{ct} is not a valid cell type. Valid cell types: {valid_cell_types}'
            logger.error(error_msg)
            raise argparse.ArgumentTypeError(error_msg)
        return ct

    description = textwrap.dedent('''
    This script performs organoid analysis of a stack file by calling the necessary downstream functions.
    It takes as input a single czi file.
    ''')
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=description)
    requiredNamed = parser.add_argument_group('Required named arguments')
    requiredNamed.add_argument('-i', '--input_stack', help='Input stack file',
                               required=True, type=validate_arg, metavar='PATH')
    requiredNamed.add_argument('-c', '--cell_type', help='cell type (e.g. Barretts)', required=True,
                               type=validate_cell_type, metavar='CELL_TYPE')
    requiredNamed.add_argument('-w', '--workdir', type=validate_arg, metavar='PATH', required=True,
                        help='Analysis working directory')
    parser.add_argument('-f', '--focus_threshold', type=int, metavar='INT', help='Focus threshold value, default: 19',
                        default=19)
    args = parser.parse_args()

    args.input_stack = Path(args.input_stack).resolve()
    args.workdir = Path(args.workdir).resolve()

    return args


def configure_logger(logger, args):
    logger.setLevel(logging.DEBUG)

    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)

    now = datetime.datetime.now()
    date_time = now.strftime("%Y%m%d_%H%M%S")
    file_handler = logging.FileHandler(
        args.workdir / Path(f"{date_time}_organoid_analysis.log"))
    file_handler.setLevel(logging.INFO)

    log_format = logging.Formatter(
        '%(asctime)s - %(levelname)s - %(module)s:%(funcName)s - %(message)s')
    console_handler.setFormatter(log_format)
    file_handler.setFormatter(log_format)

    logger.addHandler(console_handler)
    logger.addHandler(file_handler)


if __name__ == "__main__":
    main()
